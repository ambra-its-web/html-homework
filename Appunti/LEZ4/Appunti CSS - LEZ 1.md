
Si occupa di **stile e layout**, e, come Javascript, di *separare le responsabilità* all'interno della Web Page.

### Anatomia di un sito:
HTML - Struttura <br>
**CSS - Presentazione** <br>
JS - Logica/Interazione

Sta per ***Cascading Style Sheets***
- Per Cascading si intende che ogni proprietà appartiene ad un contenitore, applicandosi ai suoi elementi, e ai loro figli di conseguenza. Non tutte le proprietà sono applicabili a tutti gli elementi.
- Sheets perché sì

### Anatomia di CSS:
- css consiste di *regole di stile*
- un *blocco di codice* css è una **regola**
- ogni regola di stile consiste di un selettore e di dichiarazioni di **coppie proprietà-valore**
- una coppia *proprietà-valore* è una **dichiarazione**

~~(blocco di codice slide 6)~~
Il selettore seleziona un elemento, qui `body`, e applica una regola ad esso.

Ovviamente si usa sempre un file css esterno(meglio per la cache, per non ripeterlo in ogni .html etc), (tranne che nel primo esercizio) rispetto a inline (nel body in un attributo) ed embedded (nell'head). meglio embedded che inline, perché lavora come un compiler css.

---------------
## Selettori:
Non applica le proprietà, quello lo fa il browser.
Lui passa su tutto il file html e per ogni elemento decide se selezionarlo o no. Dice al browser quindi cosa considerare.
Confronta 

#### Element(/Tag) Selectors:
```
p {
	property: value; //non è valido, solo esempio
}
```
Seleziona tutti gli elementi p (paragraph)

```
img {
	property: value;
}
```
Seleziona tutti gli elementi img


#### Relational Selectors:
```
p em {
	color: yellow;
}
```
Seleziona solo gli em nei p, ad esempio **NON** negli h2!

Possono essere più complessi, tipo:
```
ul li a strong {
	color: purple;
}
```
Quella di "*elemento in altro elemento*" è solo una delle possibili relazioni, e una delle più comuni.

-------------------------------------
### Reusing Code: DRY
Don't Repeat Yourself.
Astrarre per eliminare la ripetizione. (~~fun fact:~~ il br si usa in poesia e lyrics, ovvero negli a capo necessari al contenuto)
Si riutilizza CSS con ID e classi, con selettori anche molto "sofisticati" in aiuto.

------------------------
### ID vs Classe

ID
- Deve essere univoco ad un elemento della pagina
>   Può esserci un solo footer in una pagina
- Usa il simbolo `#`

Classe
- Più elementi possono avere la stessa classe
>   Ci possono essere più warning in una pagina
- Usa il simbolo `.`

Posso pure usare entrambi o selettori avanzati (U5) nel caso non siano la scelta corretta.

------------------------------------------
### ID SELECTOR

nel css
```
#site-footer {
	property: value;
}
```
questo # seleziona l'elemento con id site-footer, non è il fragment di html, né correlato a js

nell'html:
`<p id="site-footer">Copyright message</p>`

------------------------

### CLASS SELECTOR

nel css:
```
.warning {
	color: red;
}
```

nell'html:
```
<p class="warning">Run away!</p>     <!-- primo elemento della classe warning -->

<div class="warning">                <!-- secondo elemento della classe warning -->
  this is also a warning
</div>

<ul>
  <li>
    <p class="warning">Danger</p>    <!-- terzo elemento della classe warning -->
  </li>
</ul>
```

----------------------

### GROUPING SELECTORS

Raggruppiamo più selettori sotto una stessa regola:
```
h3, .message, #notificationArea {
	color: Maroon
}
```

oppure
```
h3,
.message,
#notificationArea {
	color: Maroon;
}
```
Questo soprattutto se raggruppo tanti selettori, ordina il codice

-------------------------------
### PROPERTY: color & background-color

La proprietà colore cambia il colore del testo

```
p {
	color: red;
	color: #ff0000;
	color: rgb(255, 0, 0);
}
```
Base esadecimale!
256 (16x16) possibilità ad ogni casella nell'rgb, rappresentata da due cifre sulle 6 dell'esadecimale.

```
p {
	background-color: red;
	background-color: #ff0000;
	background-color: rgb(255, 0, 0);
}
```
Stessa cosa col background-color, ma applicato allo sfondo e non al testo.

--------------------

### CSS Color Values

In realtà c'è un quarto valore, l'alfa, ovvero quanto è trasparente il colore

I browser accettano:

| Color name         | red                                         |
| ------------------ | ------------------------------------------- |
| valore esadecimale | #ff0000 <br>#ff0000ff                       |
| valore RGB         | rgb(255, 0, 0) <br>rgba(255, 0, 0, 1)       |
| valore HSL         | hsl(0, 100%, 50%) <br>hsla(0, 100%, 50%, 1) |
[Cazzeggia qui](https://www.w3schools.com/colors/colors_picker.asp)

Gli altri standard:
##### HSL 
(Hue, Saturation, Luminance). **Hue** scorre lungo la barra arcobaleno. [0° e 360°] sono rosso.  Saturation è la "pienezza" del colore, diciamo quanto colore c'è oppure quanto grigio c'è [da dx-saturo-(100%) a sx-non saturo-(0%)]. Luminance dice quanto nero o no c'è [da sopra colore pieno-(100%) a sotto, nero-(0%)]

HSV gli è simile, CMYK si usa per la stampa (Ciano, Magenta, Giallo, Nero)

[I colori in testo di CSS](https://147colors.com/)

Perché un colore si definiva "web safe"?
---
Negli anni 90 molti monitor non supportavano molti colori, molte tonalità di rosso erano mostrate come lo stesso colore effettivo. Si usavano quindi colori specifici spesso, un paio di centinaia di colori considerati "sicuri" nella loro resa per l'utente.

--------------------
### Property: width

Imposta la larghezza di un elemento block o img. Quella di base è settata da regole di una stylesheet default del browser.

Non applicabile agli elementi inline, perché occupa solo l'effettivo spazio necessario per il suo contenuto (gli elementi block occupano il massimo se non specificato)

Accetta diverse unità di misura:

- **px**:
	con px del web non si intende sempre quelli del monitor. Quelli della scuola ad esempio avranno una densità di pixel 1:1, corrispondendo quindi a quelli del web. 
	Ma su mobile sarà almeno 2, 3 o 4:1 (alta densità di pixel). Se do ordine di disegnare un pixel, ne disegna coi suoi reali almeno 4 o 9.
	
	>conviene quando l'elemento necessita di una dimensione fissa e precisa, a prescindere da altro, soprattutto su specifica del designer


- **em**:
	la rende relativa alle dimensioni del primo font relativo all'elemento
 	*rem:*
    si basa su un font di un elemento root, uno sempre fisso (di base è l'elemento html)
    
    >conviene quando voglio che il testo abbia più spazio su ridimensionamento



- **%**:
	percentuale rispetto al contenitore genitore, che però deve avere una larghezza impostata (sennò 20% di niente è niente). uno dei modi per fare web responsive (mobile etc)
	
	>conviene quando un elemento dipende dal genitore


- **vw**:
	il viewport è la finestra o spazio attraverso cui l'utente vede il sito.
	es: 1vw = 1% viewport width.
	
	>conviene quando devo considerare le dimensioni della finestra