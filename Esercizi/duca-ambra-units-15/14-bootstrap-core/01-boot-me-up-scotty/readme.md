## Author
- **Name:** Ambra

## Exercise: Boot me up Scotty

### Requirements
- Study all the external links in the presentation, especially Bootstrap
components
- Create a small site that should
  - Contain at least 4 different pages
  - Demonstrate that you understand Bootstrap
  - Use real content, not placeholders
  - Use at least 10 different bootstrap components
  - At least one of the following types of components
    - Show and hide, Buttons, Content, Dialog-like, Navigation, Information, Feedback UI
  - At least one example of
    - cards with navigation headers
    - tabbed panes of content
- Include your html, css and js files in the delivery

### Files
- [index.html](./index.html): Contains the HTML structure of the Home
- [about.html](./about.html): Contains the HTML structure of the About page
- [services.html](./services.html): Contains the HTML structure of the Services page
- [contact.html](./contact.html): Contains the HTML structure of the Contact page
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of all the website's pages
- [favicon.ico](./favicon.ico) in the root directory as good practice