# Explanation of CSS Methods for "A Site About Eclipses"

This document provides an overview of different CSS methods and their applicability for a small website.

## CSS Methods:
### Reset CSS Method
The reset CSS method aims to nullify browser default styles for HTML elements, providing a fully clean slate for styling. It sets margins, padding, font sizes and more to predefined values or zero. This method ensures consistency across browsers but might be overkill for small projects.

### Destyle CSS Method
The destyle CSS method selectively removes intrusive browser styles while retaining basic styles for readability and accessibility. It strikes a balance between resetting styles and maintaining usability, making it suitable for small projects where simplicity is key.

### Normalize CSS Method
The normalize CSS method aims to make browser styles more consistent across platforms by providing a set of consistent styles that override browser defaults. It preserves useful defaults while addressing inconsistencies, but may be more complex compared to reset or destyle approaches.

## Easiest application:
Considering the end result is the same on all web browsers, for my simple website simplicity and ease of implementation are the main goals. Considering the small scale of the project, the reset CSS method is tge most straightforward option, providing control over styling without any complications. However, if I had used a slightly complex mix of predefined styles and custom ones, the destyle method could also have been a good solution, while the normalize one would have been harder to handle, but would have probably produced the most optimal result.