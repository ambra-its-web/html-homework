# CSS diner answers
* Write all the solutions that you have found
* For each solution, write the name/type of the all selectors
* For each solution, explain how the selector works and which HTML tags it targets
---

### Levels:
- 1:
    * `plate`
        - type selector, selects all elements of type "plate"

- 2:
    * `bento`
        - type selector, selects all elements of type "bento"

- 3:
    * `#fancy`
        - id selector, selects all elements with id="fancy". However, it's ill advised to give the same id to multiple elements

- 4:
    * `plate apple`
        - descendant selector, selects all "apple" elements that are inside of any "plate" element

- 5
    * `#fancy pickle`
        - id selector combined with descendant selector, selects all "pickle" elements that are inside of an element with id="fancy"

- 6
    * `.small`
        - class selector, selects all elements with class="small". It's the correct way to select multiple unrelated elements, instead of giving them a shared id

- 7
    * `orange.small`
        - type selector combined with a class selector, selects all "orange" elements with class="small"

- 8
    * `bento orange.small`
        - type selector combined with a class and descendant selector, selects all "orange" elements with class="small" inside a "bento" element

- 9
    * `plate, bento`
        - type selectors combined with a comma combinator, selects all "plate" and "bento" elements

- 10
    * a) `*`
        - universal selector, selects all elements. Also selects the "div" element with class "table"
    * b) `.table *`
        - class selector combined with a universal selector, selects all elements within table. Although this isn't an accepted answer, the visual cue suggests the table shouldn't be selected.

- 11
    * `plate *`
        - type selector combined with a descendant and a universal selector, selects all elements within the "plate" element

- 12
    * `plate + apple`
        - adjacent sibling selector, selects all "apple" elements that directly follow a "plate" element. On a markup level in the HTML file, it means they should have the same indentation level (also helps with visual clarity)

- 13
    * `bento ~ pickle`
        - general sibling selector, selects all "pickle" elements that follow a "bento" element

- 14
    * `plate > apple`
        - child selector, selects all "apple" elements that are direct children of a "bento" element

- 15
    * `plate :first-child`
        - first child pseudo-selector combined with a descendant selector, selects all first child elements within a "plate" element, in this case the top "orange" element

- 16
    * `plate :only-child`
        - only child pseudo-selector, selects all elements that are the only child of the element "plate", in this case the "apple" and "pickle" in different "plate" elements, ignoring the two "orange" elements since none are only child

- 17
    * `apple:last-child, pickle`
        - last child pseudo-selector and a type selector combined through a comma combinator, selects all last-child "apple" and all "pickle" elements

- 18
    * a) `nth-child(3)`
        - Nth child pseudo-selector, in this case it selects the third child "plate" element of the "table" element
    * b) `plate:nth-child(3)`
        - Nth child pseudo-selector, here it's specified which child element is wanted, in this case the "plate" element

- 19
    * `bento:nth-last-child(3)`
        - Nth last child pseudo-selector, selects all third-to-last child "bento" elements. the element type is specified to avoid selecting the third-to-last "orange" in one of the "plate" elements

- 20
    * `apple:first-of-type`
        - first of type selector, selects the first a"apple" element

- 21
    * `plate:nth-of-type(even)`
        - Nth of type selector, in this case it selects all even instances of a the "plate" element

- 22
    * `plate:nth-of-type(2n+3)`
        - Nth of type selector with formula, in this case it selects all 2nd instances of a the "plate" element, starting from and including the 3rd one

- 23
    * `plate apple:only-of-type`
        - only of type selector, selects the "apple" element if it's the only one of its type in a "plate" element

- 24
    * `orange:last-of-type, apple:last-of-type`
        - last of type selectors, the first one selects the last "orange" element of its type and the second one the last "apple" element of its type

- 25
    * `bento:empty`
        - empty selector, selects all empty "bento" elements

- 26
    * `apple:not(.small)`
        - negation pseudo-class selector, selects all elements that do not have class="small"

- 27
    * `[for]`
        - attribute selector, selects all elements that have a 'for="anything"' attribute

- 28
    * `plate[for]`
        - attribute selector combined with a type selector, selects all "plate" elements that have a 'for="anything"' attribute

- 29
    * `[for="Vitaly"]`
        - attribute value selector, selects all elements with the value "Vitaly" for their attribute "for"

- 30
    * `[for^="Sa"]`
        - attribute starts with selector, selects all elements with the attribute "for" which have a value that starts with "Sa"

- 31
    * `[for$="ato"]`
        - attribute ends with selector, selects all elements with the attribute "for" which have a value that ends with "ato"

- 32
    * `[for*="bb"]`
        - attribute wildcard selector, selects all elements with the attribute "for" which have a value that contains "bb" anywhere