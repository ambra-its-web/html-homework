## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: We mean business

### Requirements
- Create a table with details of business contacts.
  - Columns can include: name, email address, country, etc (max 6 columns).
  - Fill the table with contents.
- Add a styles.css file. The HTML file should not contain any inline styles!
- The table header should have a background color.
- The table rows should have an alternating white/grey background.
  - Row 1 grey, row 2 white, row 3 grey, etc.
- When the user hovers over a row with the mouse, the color should change.
- Bonus: modify your CSS so that the table row colors alternate every 2 rows.

### Approach to Solution
In `index.html`, I:
- Created a table with the required columns: Name, Email Address, Country.
- Filled the table with sample contact details.

In `style.css`, I:
- Set the table header background color to a specific shade.
- Used CSS to apply zebra stripes to the table rows, alternating their background colors between grey and white.
- Added a hover effect to change the background color of rows when hovered over.
- Implemented a bonus feature to make the table row colors alternate every 2 rows.