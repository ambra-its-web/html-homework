# Selector Specificity Analysis

## Brief overview of specificity rules:
A weight is applied to a CSS declaration, determined by the number of each selector type
- 1-0-0:
ID selector
- 0-1-0:
Class selector, Attribute selector, Pseudo-class
- 0-0-1:
Element Selector, Pseudo-element
- 0-0-0:
Universal selector (*), combinators (+, >, ~, ' ', ||) and negation pseudo-class :not()

## Breakdown of the specificity for each selector:
Below is a breakdown of the specificity for each selector, and its "rank" in how specific it is. In case multiple selectors share the same specificity, the last one in the stylesheet will override the other(s).

1. `ul li {}` - Specificity: 0-0-2 (**9th**)
   - Targets all `<li>` elements within `<ul>` elements. Specificity: 0-0-2 (Element Selector).

2. `ul > li {}` - Specificity: 0-0-2 (**12th**)
   - Selects direct `<li>` children of `<ul>` elements. Specificity: 0-0-1 (Element Selector).

3. `body > #main.mobile a:hover {}` - Specificity: 0-2-2 (**5th**)
   - Identifies `<a>` elements nested within `#main.mobile` in the `body` on hover. Specificity: 0-2-2 (ID and Class Selectors, Pseudo-class).

4. `div p > span {}` - Specificity: 0-0-3 (**1st**)
   - Picks out `<span>` elements inside `<p>` within `<div>` elements. Specificity: 0-0-3 (Element Selector).

5. `.users .name {}` - Specificity: 0-2-0 (**7th**)
   - Targets elements with class `.name` within `.users`. Specificity: 0-2-0 (Class Selectors).

6. `[href$='.pdf'] {}` - Specificity: 0-1-0 (**8th**)
   - Matches elements with `href` ending in '.pdf'. Specificity: 0-1-0 (Attribute Selector).

7. `:hover {}` - Specificity: 0-0-1 (**11th**)
   - Applies styles on hover. Specificity: 0-0-1 (Pseudo-class).

8. `div .name {}` - Specificity: 0-1-1 (**6th**)
   - Selects elements with class `.name` within `<div>` elements. Specificity: 0-1-1 (Class and Element Selectors).

9. `a[href$='.pdf'] {}` - Specificity: 0-1-1 (**6th**)
   - Styles `<a>` elements with `href` ending in '.pdf'. Specificity: 0-1-1 (Element and Attribute Selectors).

10. `.pictures img:hover {}` - Specificity: 0-2-1 (**4th**)
    - Styles `<img>` elements inside `.pictures` on hover. Specificity: 0-2-1 (Class Selector and Pseudo-class).

11. `.news.breaking.featured {}` - Specificity: 0-3-0 (**3rd**)
    - Styles elements with classes `.news`, `.breaking`, and `.featured`. Specificity: 0-3-0 (Class Selectors).

12. `.user #name {}` - Specificity: 1-1-0 (**10th**)
    - Styles elements with ID `#name` inside `.user`. Specificity: 1-1-0 (ID and Class Selectors).

13. `#name span {}` - Specificity: 1-0-1 (**13th**)
    - Styles `<span>` elements within the element with ID `#name`. Specificity: 1-0-1 (ID and Element Selectors).

14. `nav#nav > li:hover {}` - Specificity: 1-1-1 (**2nd**)
    - Styles `<li>` elements inside `nav#nav` on hover. Specificity: 1-1-1 (ID, Element, and Pseudo-class).

15. `li:nth-child(2n+1):hover {}` - Specificity: 0-2-1 (**4th**)
    - Styles odd `<li>` elements on hover. Specificity: 0-2-1 (Element Selector and Pseudo-class).