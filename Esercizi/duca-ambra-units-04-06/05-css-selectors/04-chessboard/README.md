## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Chessboard

### Requirements
- Create a `<table>` with 8 rows and 8 columns.
- Ensure `<td>` elements are either empty or contain one letter when necessary.
- Use CSS selectors to generate a chessboard pattern.
- Apply the following rule to fill the table cells:
  ```css
  td {
    width: 20px;
    height: 20px;
  }

### Approach to Solution
In `index.html`, I made an 8x8. 

In `style.css`, I:
- Set the dimensions of table cells to be 20 pixels by 20 pixels.
- Used the `:nth-child()` pseudo-class to target specific cells for color alternation by using (odd) and (even) as specifier, with comma combinators.
- Applied a black background color to even rows and odd columns, and an orange background color to odd rows and even columns to create the chessboard pattern.