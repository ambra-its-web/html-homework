# Flexbox Froggy answers

### 1)
```scss
#pond {
  display: flex;
  justify-content: flex-end; // aligns items horizontally to the right side of the container. can also just type `end` as value
}
```

also:
```scss
#pond {
  display: flex;
  flex-direction: row-reverse; // this way we switch the direction of the main axis, leading to the flex default being the right side
}
```

### 2)
```scss
#pond {
  display: flex;
  justify-content: center; // aligns items so they're evenly spaced horizontally, still accounting for their margins
}
```

### 3)
```scss
#pond {
  display: flex;
  justify-content: space-around; // aligns items so they're evenly spaced horizontally, still accounting for their margins
}
```

### 4)
```scss
#pond {
  display: flex;
  justify-content: space-between; // aligns the first item at the start of the container, the last one at its end, then evenly distributes the remaining space between the other items, in this case just one item
}
```

### 5)
```scss
#pond {
  display: flex;
  align-items: end; // aligns items vertically to the end of the secondary axis
}
```

### 6)
```scss
#pond {
  display: flex; 
  justify-content: center; // aligns items to the center of the main axis
  align-items: center; // aligns items to the center of the secondary axis
}
```

### 7)
```scss
#pond {
  display: flex;
  justify-content: space-around; // aligns items so they're evenly spaced horizontally, still accounting for their margins
  align-items: end; // aligns items vertically to the end of the secondary axis
}
```

### 8)
```scss
#pond {
  display: flex;
  flex-direction: row-reverse; // switches the direction of the main axis, leading to the flex default being the right side
}
```

### 9)
```scss
#pond {
  display: flex;
  flex-direction: column; // switches the main axis to be vertical
}
```

### 10)
```scss
#pond {
  display: flex;
  flex-direction: row-reverse; // switches the direction of the main axis, leading to the flex default being the right side
  justify-content: end; // aligns items vertically to the end of the secondary axis
  // this way the frogs go onto their assigned colored lilypads
}
```

### 11)
```scss
#pond {
  flex-direction: column; // switches the main axis to be vertical
  justify-content: end; // aligns items vertically to the end of the secondary axis
}
```

### 12)
```scss
#pond {
  display: flex;
  flex-direction: column-reverse; // switches the main axis to be vertical and switches its direction
  justify-content: space-between; // aligns the first item at the start of the container, the last one at its end, then evenly distributes the remaining space between the other items, in this case just one item
}
```

### 13)
```scss
#pond {
  display: flex;
  flex-direction: row-reverse; // switches the direction of the main axis
  justify-content: center; // aligns items to the center of the main axis
  align-items: end; // aligns items vertically to the end of the secondary axis
}
```

### 14)
```scss
#pond {
  display: flex;
}

.yellow {
order: 1; // assigns an order value to one item, positive to have it move further in the order. the other 2 have the default value 0
}
```

### 15)
```scss
#pond {
  display: flex;
}

.red {
order: -1 // negative value so it moves to first position compared to the others having 0
}
```

### 16)
```scss
#pond {
  display: flex;
  justify-content: space-between;
}

.yellow {
align-self: end; // moves a specified flex item to the end of the secondary axis
}
```

### 17)
```scss
#pond {
  display: flex;
}

.yellow {
  order: 1; // moves them to main axis end in order
  align-self: end; // moves specified flex items to the end of the secondary axis
} 
```

### 18)
```scss
#pond {
  display: flex;
  flex-wrap: wrap; // allows items to move to a different row whenever the main axis is "full"
}
```

### 19)
```scss
#pond {
  display: flex;
  flex-direction: column; // switches the main axis to be vertical
  flex-wrap: wrap; // allows items to move to a different row whenever the main axis, in this case vertical, is "full"
}
```

### 20)
```scss
#pond {
  display: flex;
  flex-flow: column wrap; // shorthand property for `flex-direction` and `flex-wrap`
}
```

### 21)
```scss
#pond {
  display: flex;
  flex-wrap: wrap;
  align-content: start; // sets the spacing and position of the lines. content properties can be used only if `flex-wrap` is active. just like `flex-end`, can be shortened to `start`
}
```

### 22)
```scss
#pond {
  display: flex;
  flex-wrap: wrap;
  align-content: end; // aligns all lines vertically to the end of the secondary axis
}
```

### 23)
```scss
#pond {
  display: flex;
  flex-wrap: wrap;
  flex-direction: column-reverse; // switches the main axis to be vertical and switches its direction
  align-content: center; // aligns lines to the center of the secondary axis
}
```

### 24)
```scss
#pond {
  display: flex;
  flex-direction: column-reverse; // switches the main axis to be vertical and switches its direction.
  flex-wrap: wrap-reverse; // allows items to move to a different row whenever the main axis is "full", but orders items in reverse. This way the yellow frogs are near their colored lilypads
  justify-content: center; // here it allows the wrapped items to be in the center of the line, although all items are affected
  align-content: space-between; // lets the items be spaced from one end to the other of the container
}
```