# Flexbox Defense answers

### 1)
```scss
.tower-group-1 {
  display: flex;
  justify-content: center;
}
```

### 2)
```scss
.tower-group-1 {
  display: flex;
  justify-content: flex-end;
}
.tower-group-2 {
  display: flex;
  justify-content: center;
}
.tower-group-3 {
  display: flex;
  justify-content: flex-end;
}
```

### 3)
```scss
.tower-group-1 {
  display: flex;
  justify-content: center;
}
.tower-group-2 {
  display: flex;
  justify-content: space-between; // need the two towers to be one at the start and one at the end
}
```

### 4)
```scss
.tower-group-1 {
  display: flex;
  align-items: flex-end;
}
.tower-group-2 {
  display: flex;
  align-items: flex-end;
}
```

### 5)
```scss
.tower-group-1 {
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
}
.tower-group-2 {
  display: flex;
  justify-content: center;
}
.tower-group-3 {
  display: flex;
  justify-content: center;
  align-items: center;
}
```

### 6)
```scss
// I toggled tower-inputs on
.tower-group-1 {
  display: flex;
  justify-content: space-between;
}
.tower-1-1 {
  align-self: center;
}
.tower-1-3 {
  align-self: center;
}
```

### 7)
```scss
.tower-group-1 {
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}
.tower-group-2 {
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}
```

### 8)
```scss
.tower-group-1 {
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}
.tower-group-2 {
  display: flex;
  flex-direction: column;
  align-items: center;
}
```

### 9)
```scss
.tower-group-1 {
  display: flex;
  justify-content: space-around;
  flex-direction: row-reverse;
}
.tower-group-2 {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row-reverse;
}
```

### 10)
```scss
.tower-group-1 {
  display: flex;
  justify-content: space-around; // to evenly space the mid items accounting for margins
}

.tower-1-2 {
  order: 1; // to put the first super tower last
}

.tower-group-2 {
  display: flex;
  justify-content: space-around;
}

.tower-2-2 {
  order: -1; // to put the second super tower first
}
```

### 11)
```scss
.tower-group-1 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.tower-1-2 {
  align-self: flex-start;
}
.tower-1-3 {
  align-self: flex-end;
}
.tower-1-4 {
  align-self: flex-start;
}
```

### 12)
```scss
.tower-group-1 {
  display: flex;
  justify-content: space-between;
}
.tower-1-1 {
  order: -1;
}
.tower-1-2 {
  order: 0;
  align-self: center;
}
.tower-1-3 {
  order: 2;
  align-self: center;
}
.tower-1-4 {
  order: 1;
  align-self: center;
}
.tower-1-5 {
  order: 3;
  align-self: flex-end;
}
```