## Author
- **Name:** Ambra

## Exercise: In information

### Requirements
- Study all the external links in the presentation
- Create a complex bootstrap form that should
    - Contain at least 15 different input types
    - Use custom style validation or tooltips
    - Connected to and tested on RequestBin
    - Validation should include at least the following
        - Validation methods: Required, minlength and maxlength for text input, min and max for number input, pattern
        - Types: number, an email, date
- Include your html, css and js files in the delivery

### Approach to Solution

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage.
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles.
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage.
- [favicon.ico](./favicon.ico) in the root directory as good practice