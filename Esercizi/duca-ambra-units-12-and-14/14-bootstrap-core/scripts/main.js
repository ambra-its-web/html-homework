// Wait for the DOM to load before running the script
document.addEventListener("DOMContentLoaded", function () {

  // Store the form element in a constant
  const form = document.querySelector('form');

  /**
 * Validates the form fields and handles the submission of the form.
 * @function
 * @description This function:
 * - Validates the form fields for required fields, email, phone number, dob, email, first and last name, and zip code
 * - Displays error messages if there are any validation errors
 * - Submits the form if it's validated
 */
  function validateForm(event) {

    // Email validation
    function validateEmail(email) {
      const emailPattern = /.+@.+\..+/; // Checks if there's something before/after @ and a dot
      return emailPattern.test(email);
    }

    // Phone validation
    function validatePhone(phone) {
        const phonePattern = /^\d{7,}$/; // Checks if it's at least 7 digits long
        return phonePattern.test(phone);
    }

      // Get form field values through DOM
      const name = document.getElementById("inputname").value.trim(); // to remove whitespace
      const lastName = document.getElementById("inputlastname").value.trim();
      const email = document.getElementById("inputemail").value.trim();
      const password = document.getElementById("inputpassword").value.trim();
      const dob = document.getElementById("dob").value;
      const phone = document.getElementById("phone").value.trim();
      const zip = document.getElementById("zip").value.trim();

      // Error messages array initialization
      let errors = [];

      // Name and Last Name validation (required fields)
      if (name === "") {
          errors.push("Name is required.");
      }
      if (lastName === "") {
          errors.push("Last name is required.");
      }

      // Email validation (required)
      if (email === "") {
          errors.push("Email is required.");
      } else if (!validateEmail(email)) {
          errors.push("Invalid email format.");
      }

      // Password validation (required)
      if (password === "") {
          errors.push("Password is required.");
      }

      // Date of Birth validation (required)
      if (dob === "") {
          errors.push("Date of birth is required.");
      }

      // Phone number validation (optional)
      if (phone !== "" && !validatePhone(phone)) {
          errors.push("Phone number must be at least 7 digits.");
      }

      // Zip Code validation (required, must be number and 5 digits)
      if (zip === "" || isNaN(zip) || zip.length !== 5) {
          errors.push("Invalid or missing zip code. Must be 5 digits.");
      }

      // Show errors if there are any, otherwise submit the form
      if (errors.length > 0) {
          alert(errors.join("\n"));
      } else {
          form.submit();  // This submits the form if no errors were found
      }
  }

  // Attach the function to submit event
  form.addEventListener('submit', validateForm);

});