/**
* @file: main.js
* @author: Fabri
* Introduce logica di validazione del form attraverso la funzione validateForm()
*/

// Coatante necessaria per correlare il tasto submit
const subbtn = document.getElementById('submit');

// funzione validateForm
// Funzione che controlla la validità dei dati inseriti e se quelli obbligatori siano inseriti per permettere l'invio del form
/**
 *  Ritorna true/false, per validare o no il form
 * @param  {string} nome - stringa contenente il nome dell'utente
 * @param  {string} cognome - stringa contenente il cognome dell'utente
 * @param  {boolean} genere_maschio - primo possibile genere specificato nei radio button
 * @param  {boolean} genere_femmina - secondo possibile genere specificato nei radio button, attivo di default per via della probabilità più alta che l'utente sia femmina globalmente
 * @param  {boolean} genere_unspec - terzo possibile genere specificato nei radio button
 * @param  {string} email - email dell'utente, formato controllato dal regex
 * @param  {num} eta - età dell'utente, minimo 18 anni, massimo 99
 * @param  {string} esperienza - stringa in cui l'utente digita la propria esperienza lavorativa
 * @param  {string} istruzione - stringa in cui l'utente digita il proprio percorso di studi
 * @param  {string} motivazione - stringa in cui l'utente digita le motivazioni per la candidatura
 
 * @returns {boolean} true/false - indica se il form sia compilato correttamente o no
 */
function validateForm() {
    let nome = document.getElementById('nome').value;
    let cognome = document.getElementById('cognome').value;
    let genere_maschio = document.getElementById('genere_maschio').checked;
    let genere_femmina = document.getElementById('genere_femmina').checked;
    let genere_unspec = document.getElementById('genere_unspec').checked;
    let email = document.getElementById('email').value;
    let eta = document.getElementById('eta').value;
    let esperienza = document.getElementById('esperienza').value;
    let istruzione = document.getElementById('istruzione').value;
    let motivazione = document.getElementById('motivazione').value;

    // validazione campo mancante
    if (nome === '' || cognome === '' || email === '' || eta === '' || esperienza === '' || istruzione === '' || motivazione === '') {
        alert('Compilare tutti i campi.');
        return false;
    }

    // validazione email
    let emailRegex = /^\S+@\S+\.\S+$/; // controllo della validità dell'email sfruttando Regex
    if (!emailRegex.test(email)) {
        alert('Inserire un\'email valida.');
        return false;
    }

    // validazione età
    if (isNaN(eta) || eta < 18 || eta > 99) {
        alert('Inserire un\'età valida (compresa tra 18 e 99).');
        return false;
    }

    // validazione genere
    if (!genere_maschio && !genere_femmina && !genere_unspec) {
        alert('Selezionare un genere.');
        return false;
    }

    return true;
}

subbtn.addEventListener('click', validateForm)