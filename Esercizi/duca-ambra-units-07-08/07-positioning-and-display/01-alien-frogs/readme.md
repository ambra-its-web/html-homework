## Author
- **Name:** Ambra Duca

## Exercise: Alien frogs

### Requirements
- On an alien planet you find alien frogs
  - Two types: green and orange
  - And two sizes: big and small
  - Small frogs have dark eyes
  - Big frogs have bright eyes
  - When frogs are touched their eyes become black
- See next page for technical details
- See frogs.gif animation for behaviors
- Create a page full of all possible combinations of alien frogs
- Frogs should fill the page from left to right
- Frogs should wrap if there isn't enough space
- All frogs should change their eye color to black when touched
- The HTML should be valid and complete
- In the readme.md explain your CSS code
- Do all the work in CSS, the HTML should only include elements of this type:

  ```css
  <div class="frog type1 small">--</div>
  <div class="frog type2 small">--</div>
 <!-- etc...  -->

- Extra requirement: after 5 seconds, the frogs do something.

### Approach to Solution
In `index.html`, I made 10 divs with different classes: 
- `frog`, applied to all
- `type1` and `type2`, applied to half and half of the divs respectively
- `small`, applied to half the divs

---

In `style.css`, I:

- Applied a background color and some padding to the body
- Gave the "global" .frog class the base properties:
  - `display: inline-block` so that the frogs align horizontally and move down when the viewport width requires it
  - Dimensions and style that roughly match the example provided
  - `position: relative` so that the positioning of the pseudo-elements will be based on their parent div's

- Gave `type1` frogs a green background color

- Created pseudo-elements before and after for `type1` frogs to make the eyes:
  - `content: ○` for the pupil
  - 50% border radius to start making them circular
  - Absolute positioning, based on the parent div
  - Position properties, 75% bottom and -15% left for the `::before`, -15% right for the `::after`, and a width of 20px to make it more circular

- Gave `type2` frogs an orange background color

- Created pseudo-elements before and after for `type2` frogs to make the eyes. Same process of `type1`

- Gave `small` class divs different dimensions

- To affect the pseudo-elements' styling upon hovering, I:
  - gave `.frog:hover:before` a different background color, a different text color, and the pointer cursor
- To get the hover pointer cursor when hovering the div itself too I gave it just the cursor pointer property

The remainder of the rules will be explained in the [extra CSS section](#extraCSS) right after the JavaScript one.

---

#### Extra: Make the frogs do something after 5 seconds
In `main.js` I changed the DOM elements after a specified time by:
- Defining a function frogsAct that:
  - Adds an ID to the body
  - A class to all div elements
 - Scheduling the function to be invoked in no less than 5 seconds

---

<a id="extraCSS">Back in `style.css`</a>, I added some rules that apply after the javascript function that adds the classes is executed, these are:
- Changes to the color of the divs
- Changes to the content of the pseudo-elements
- A background image

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage.
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles.
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage.
- [favicon.ico](./favicon.ico) in the root directory as good practice