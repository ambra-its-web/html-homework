/**
 * @file: main.js
 * @description: Additional part of the exercise
 * This script changes the DOM elements after a specified time.
 * Specifically, it:
 * - Defines a function frogsAct that:
 *  - Adds an ID to the body
 *  - A class to all div elements
 * - Schedules the function to be invoked in no less than 5 seconds
 */

/**
 * Function that adds an ID to the body and a class to all div elements
 * - Sets the attribute id of the body element to 'sleep'
 * - Assigns the frogs variable the nodelist of all divs in the page
 * - Iterates through the node array and adds to each element of it the 'action' class
 */
function frogsAct() {
    // Setting the ID of the body to 'sleep'
    document.body.setAttribute('id', 'sleep');

    // Selecting all div elements through queryselection
    let frogs = document.querySelectorAll('div');

    // Iterating over each div element and adding the class 'action' to each one
    frogs.forEach(function(frog) {
        frog.classList.add("action");
    });
}

// Set a timeout to invoke frogsAct after no less than 5 seconds
setTimeout(frogsAct, 5000);
