## Author
- **Name:** Ambra

## Exercise: Columns Columns

### Requirements
Create a web page that includes:
- A header, footer, sidebar, and two main columns.
- Use `position`, `float`, and `margins` to create the layout.
- The header and footer should stay in place when the page scrolls.
- The sidebar and two columns should fill the rest of the page, both vertically and horizontally, and should expand proportionally when the window is resized.
- One of the columns should contain an image and a caption placed on top of the image.
- Use semantic HTML5 elements.
- Fill the page with meaningful content.

### Approach to Solution
- To make the header and footer stay fixed at the top and bottom of the page while scrolling I used `position: sticky`, applied to them through the class `sticky`, where I also gave them a z-index of 2 to avoid other elements from overlapping the header while scrolling.

- Dotted border style was applied to all containers by using the class `container`

- The sidebar was floated to the left, while the sections were floated one to the left too and the other to the right, each assigned a width of 40%, with the sidebar taking up 20%. This allows the layout to fill the remaining space and expand when the window was resized.

- One of the main columns contains an image with a caption placed on top. The image and caption were placed inside a section with ID `imgContainer`. I applied different padding to make it stand out from the `p` elements above and below it, and the font size was adjusted to maintain readability.

- To keep the requirement of using semantic elements, I used `<ul>` with list-style set to none to add padding to each anchor's contextual text in the sidebar.

- The pseudo-class :nth-child was used to change the color of the text in different spans in the last anchor.

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage.
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles.
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage.
- [favicon.ico](./favicon.ico) in the root directory as good practice