## Author
**Name:** Ambra Duca

## Exercise: United nations

### Requirements
- Make a scrollable `<ul>` list of at least 50 countries
    - Each list item should contain: the flag, the name and the population number
    - The name should be left aligned and the population should be right aligned
    - Use position, float and margins to create the layout
    - Countries with large populations should be highlighted in some way
    - The flags area should have a background color
    - If the flag is not rectangular (e.g. Switzerland) the flag should be centered in the flag area
    - Include at least one country with an irregularly sized flag (e.g Switzerland)
    - Include a header above the list
- It should contain the following titles: 'Flag', 'Country', 'Population'
    - It should be spaced and aligned properly
    - It should remain in place when the list is scrolled
    - Not to be confused with the `<header>` tag

### Approach to Solution
In `index.html`, I structured the HTML with:
- A `main` container that holds the entire list with id "container" for convenience
- A `div`that contains the column headers with id `header` for convenience
    - 3 `divs` within the header div with class `header-item` and text content "Flag", "Country" and "Population"
- A `ul` element containing 50 `li` elements, each representing a country.
    - I assigned the class `row` to each `li` for convenience, since it acts like a table. 
    - If the country's population is greater than 20M, I manually gave the list item the class `large-population`.
    - Each `li` element,  contains 3 `div` elements, each containing the country's:
        1) Flag image, class `flag-area`, contains an `img` element with the [flagpedia site](https://flagpedia.net/download/api) as resource location, which gives a different SVG format image through the suffix format `/xx.svg`, the first two characters to be replaced with the listed country's two letter code
        2) Name, class `country-name`
        3) Population, class `population`. As previously stated, if it's greater than 20M I assigned the list item the proper class.
---
In `style.css`:
- General `body` styling
- Centered the main through its `container` id and it occupies 80% of the width
- Made the `header` id div:
    - Sticky with top positioning 0 so it stays at the top while scrolling
    - Have a `z-index` of 2 to prevent the flag images from overlapping with it
    - Have general styling
- Gave the `header-item` class elements:
    - Float to the left to align them
    - Equal width in percentage
    - Text align center so there's only two rules needed to align the other 2 header items' text
    - Used pseudoclasses `:nth-of-type` to select the `header-item` class items to align their text singularly. Made the middle one 34% width to fill the gap left by dividing 100% by 3
- Gave `country-list` class elements relative positioning.
- Display list items as blocks and giving them the `clear` property set to both to simulate row behaviour
- Floated flag containers of class `flag-area` left, set width same as the flag header and base background color with alpha channel, set position relative to allow the flags to position relative to their containers
- Centered flag images within their containers of class `flag-area` through absolute positioning, limited dimensions so they don't overflow their row and the column
- Floated `country-name` class divs left, centers inline element vertically through `line-height`, adds left padding.
- Floated `population` class divs right, the rest same as the country names. Omitting width here allows the text to automatically align right.
- Highlighted countries with large populations using background color based on their class `large-population`

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage.
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles.
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage.
- [favicon.ico](./favicon.ico) in the root directory as good practice

#### [02-united-nations-table](../02-united-nations-table/)
It's a previous version of the exercise which didn't meet the assignment requirements, something I realized after completing it. It involves the use of an actual table instead of a replication of it through lists.