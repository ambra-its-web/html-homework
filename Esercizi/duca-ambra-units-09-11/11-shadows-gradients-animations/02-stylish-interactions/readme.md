## Author
- **Name:** Ambra

## Exercise: Stylish interactions

### Requirements
Create a web page to showcase various CSS effects
- Include a heading and a paragraph with some sample text
- Apply a linear gradient background to the heading
- Add a shadow to the paragraph text
- Use a transition to smoothly change the paragraph text color on hover
- Create a div and apply a transformation to rotate it 45 degrees
- Add an animation to the div that moves it from left to right continuously
Bonus: use radial gradient for the heading background and add a hover effect to 
pause the animation


### Approach to Solution

**In the HTML** I:
- Created a basic structure with:
  - a heading `<h1>`
  - a paragraph `<p>`
  - a div element with the class `.cube` 
To demonstrate various CSS animation effects

**In the SCSS** I:
- Defined general styling rules for the `html` and `body`
- Created color variables to avoid repetition
- Applied a linear gradient background to the heading `<h1>` from left to right using the variables
- Styled the paragraph `<p>` with a text shadow and a color transition on hover
- Styled the `.cube` class div with specific size, background color, position, font size, text color, margins, and border radius, using variables
- Added an animation to the `.cube` div that moves it from left to right continuously with rotation
- Implemented a hover effect to pause the animation when the cursor is over the `.cube` div

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles
- [style.scss](./styles/style.scss): Contains SCSS styles from which the CSS is generated
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage
- [favicon.ico](./favicon.ico): Favicon included in the root directory asgood practice