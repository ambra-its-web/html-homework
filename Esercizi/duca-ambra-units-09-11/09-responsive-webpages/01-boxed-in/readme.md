## Author
- **Name:** Ambra

## Exercise: 

### Requirements
- Create a fully responsive page
- The page should show a different image at each breakpoint
- Pick 5 good breakpoints and change images using media queries
- Make interesting changes at each breakpoint using the CSS properties that you know
- Be creative and make a cool sequence of images
- One idea would be to create a sequence of pictures showing a person in a shrinking space
- note: you don't need the "Responsive images" section

---

# Disclaimer:

**It is recommended to view this exercise's results by using the Device Toolbar toggle set to _Dimensions: Responsive_**

---
### Approach to Solution
- In the HTML:
    - I used a `main` as a container for two divs:
        - `#anim-zone`, where a frog image is displayed through media queries.
        - `#platform`, whose position will be changed by two queries to simulate the frog in the image jumping.

- In the CSS:
    - General style was applied to the body, and I centered the main with `margin: auto`, while giving it absolute position to allow the platform to move in the queries.
    - I gave `#anim-zone` em dimensions to keep the size of the image constant as the viewport rearranges it, used `background-size: cover` to prevent the image from duplicating to fill the space it's given since it's a small file, and auto margins to center it since it won't be moved and doesn't need positioning for that.
    - I gave `#platform` general style, relative position and a specified distance so that it sticks to the image that can be seen, since the PNG occupies more space than displayed.

- **Queries**:
    1) _First query_:
        - minimum width of 2000px _and_ 100vw maximum width, the latter so it's the default one when the viewport width is maximized. 
        - The image displayed is [1Jump.png](./assets/1Jump.png). 
        - `#platform`'s distance from the bottom is reduced to simulate the frog's jump height while keeping the image in the middle of the horizontal view.
        - The body switches background color to a pale pink.

        1.5) ~~Intermediary query:~~
        - Its purpose is just to ease the next "frame"'s transition.
        - min width of 1500px _and_ 1999 max.
        - The image displayed is still [1Jump.png](./assets/1Jump.png).
        - The body switches background color to a blu-ish purple.

    2) _Second query_ 
    - min width of 1100px _and_ 1499 max.
    - The image displayed is [2-Land1.png](./assets/2-Land1.png).
    - Here `#platform` returns to its default position and the body regains the background color set in the CSS.

    3) _Third query_ 
    - min width of 900px _and_ 1099 max.
    - The image displayed is [3-Land2.png](./assets/3-Land2.png).

    4) _Fourth query_ 
    - min width of 750px _and_ 899 max.
    - The image displayed is [4-Idle.png](./assets/4-Idle.png).

    5) _Fifth query_ 
    - min of 1px _and_ 749 max, the former so it's the default one when the viewport width is as small as possible. 
    - The image displayed is [5-Croak.png](./assets/5-Croak.png).

### Files
- `index.html`: Contains the HTML structure of the webpage.
- `reset.css`: Contains CSS rules to reset default browser styles.
- `style.css`: Contains CSS styles for the layout and presentation of the webpage.
- The `\assets` folder contains the 5 images, each a frame of a sprite.