// Assigning the variable hamburger the element id #hamburger from the html. It'll be used as an event listener
let hamburger = document.querySelector("#hamburger");

/* Assigning the nav variable the .nav-menu class from the html to affect the css */
let nav = document.querySelector(".nav-menu");

/* Assigning the bar variable the .bar class from the html to affect the css */
let bars = document.querySelectorAll(".bar");

/** Toggles menu visibility and placement
 * Toggles on and off the nav-active class and its
 * css rules, and the hamburger-active class.
 * Used as an argument for the event listener
 */
function toggler() {
    nav.classList.toggle("nav-active");
    for (let i = 0; i < bars.length; i++) {
        bars[i].classList.toggle("hamburger-active");
    }
}
// Sets a boolean value that toggles on and off the CSS ruleset the respective variable was initialized to
hamburger.addEventListener("click", toggler);