## Author
- **Name:** Ambra

## Exercise: Full responsive page

### Requirements

### Files
- `index.html`: Contains the HTML structure of the webpage.
- `reset.css`: Contains CSS rules to reset default browser styles.
- `style.css`: Contains CSS styles for the layout and presentation of the webpage.
- `main.js`: Contains code that allows the CSS rulesets to apply based on user interaction.
- The `\assets` folder contains the 5 images, each a frame of a sprite.