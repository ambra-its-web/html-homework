## Author
- **Name:** Ambra

## Exercise: 

### Requirements
- In 02-responsive-menu create 2 subfolders called responsive-menu-a and responsive-menu-b
- Each folder should have a readme.md, style/style.css and index.html
- For responsive-menu-a, use max-width media queries only
- For responsive-menu-b, use min-width media queries only
- Ensure the menus are semantic, responsive, and include at least 7 items

---

### Approach to Solution
- In the HTML:
    - 
- In the CSS:
    - 

### Files
- `index.html`: Contains the HTML structure of the webpage.
- `reset.css`: Contains CSS rules to reset default browser styles.
- `style.css`: Contains CSS styles for the layout and presentation of the webpage.
- `main.js`: Contains code that allows the CSS rulesets to apply based on user interaction.
- The `\assets` folder contains the 5 images, each a frame of a sprite.