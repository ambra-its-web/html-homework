## Author
- **Name:** Ambra

## Exercise: Mix it up

### Requirements
- Write a mixin that uses another mixin that uses yet another mixin
- All 3 mixins should accept parameters and do something useful
- Create a complete page with a few SCSS features and variables
- Use the 3 mixins that you created in a useful way in the page
- Submit your SCSS, CSS and HTML files as well as any files used for generating the SCSS

---

### Approach to Solution

- In HTML:
    - Added a link to the `style.css` file
    - Added a link to the `favicon.ico` file
    - I put an `<h1>` within a `<header>` element with class `.text`
    - Added 3 `<div>` elements, the last one having a `<strong>` element inside
    - Finished with a `<footer>` element with class `.text`.
    

- The SCSS file:
  - General `<body>` styling
  - Uses 2 global size variables, `$minimal` and `$frame`
  - Assigns the `.text` class element `padding` with `$minimal` as value

  - Used a first `@mixin`, calling it `bareBones`, which:
    - Assigns to `border` the `$frame`, `solid` and `black` values
    - Assigns to `margin` and `padding` the `$minimal` value

  - Used a second `@mixin`, calling it `styler`, which:
    - Has two parameters: `$aligned: right`, `$bland: white`
    - Includes the `@mixin` `bareBones`
    - Assigns to `background-color` `$bland` as value
    - Assigns to `text-align` the `$aligned` value, in this case 'right'

  - Used a third `@mixin`, calling it `sassy`, which:
    - Includes the `styler` mixin, modifying its parameter `$aligned` to the value 'center'
    - Declares variables:
      - `$size`, its value is `$minimal*2`
      - `$fraction`, its value is `1/2` **(deprecated)**
      - `$stylish`, its value is a HEX color
    - Sets `border`'s value to `$frame / fraction inset`
    - Sets `font-size`'s value to `$size`
    - Sets `line-height`'s value to `$size * $fraction`, just to 
    showcase operators more since it'll be `1em` just like `$minimal`
    - Sets `background-color` to `$stylish`

  - After the `@mixin` rules are set
  - The first `<div>` within `<body>` has the inclusion of the `bareBones` mixin as a statement
  - The second `<div>` within `<body>` has the inclusion of the `styler` mixin as a statement, so in total 2 mixins applied
  - The last `<div>` within `<body>` has:
    - The inclusion of the `sassy` mixin as a statement, so in total 3 mixins applied
    - A nested `<strong>` element selector that states `font-weight` set to `700`


### Files
- [index.html](./index.html): Contains the HTML structure of the webpage
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser's user agent styles.
- [style.scss](./styles/style.scss): Contains the SCSS styling that generates the site's CSS file
- [style.css](./styles/style.css): Contains CSS generated from an SCSS file for the styling of the page, along with being the main bulk of the exercise
- [favicon.ico](./favicon.ico) in the root directory as good practice