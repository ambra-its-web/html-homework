## Author
- **Name:** Ambra

## Exercise: Of light and darkness

### Requirements
- Create a complete page with at least 3 styled page elements
  - e.g a page with a list, a table and a form with inputs and buttons
- Use SCSS variables, the parent selector, mixins or @extend and color functions to do the following
  - The page should have two 'themes', light and dark
  - You may use the classes on body 'light' or 'dark' to change themes
  - Use SCSS to generate the themes dynamically
  - Try to generate as much as possible changing only 2 or 3 main color values
- Submit your SCSS, CSS and HTML files as well as any files used for generating the SCSS

---

### Approach to Solution

- **In the HTML I**:
- Added hidden radio buttons for theme toggling. These radio buttons (`light-mode` and `dark-mode`) are used to switch between the light and dark themes and are hidden from view using the `theme-selector` class.
- Wrapped the main content of the page inside a `div` with the class `theme-changer`. This container is responsible for applying the theme styles.
- Included a `<header>` section with a title and navigation buttons for switching themes. The navigation buttons are implemented as labels that toggle the radio buttons.
    

- **In SCSS I**:
  - Defined an SCSS variable called `themes`
    - It contains two maps, `default` and `dark`
      - `default` contains bg color and color properties, but also styles for the `<button>` elements to showcase what happens when there's no parameters to change to
      - Dark only contains bg color and color properties
  - Defined a function called `theme` that takes two parameters and turns them into a map gathered from the map of two arguments to add: the object name and the name of its property. This is to avoid having to always get the properties manually from the map
  - Added a placeholder called `btn-style` that holds most button element styles
  - Gave the `theme-changer` container styling, including the theme function for its bg color and color
  - Hid the theme-selectors radio buttons with the `display` property
  - Changing the parameters of the `theme` function in all theme-changer and its children elements
  upon the default and dark buttons being checked
  - General `<header>`, `<nav>`, `<h1>`, <`button`>, and all content within the <`main`> styling
      - The <`h1`>, <`nav`> and <`button`> elements do not change between `default` and `dark` mode because of the lack of a parameter for the latter
      - Also used @extend of the `btn-style` placeholder to assign <`button`> style


### Files
- [index.html](./index.html): Contains the HTML structure of the webpage
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser's user agent styles.
- [style.scss](./styles/style.scss): Contains the SCSS styling that generates the site's CSS file
- [style.css](./styles/style.css): Contains CSS generated from an SCSS file for the styling of the page, along with being the main bulk of the exercise
- [favicon.ico](./favicon.ico) in the root directory as good practice