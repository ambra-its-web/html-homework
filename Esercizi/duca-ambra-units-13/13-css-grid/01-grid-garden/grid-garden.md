# Flexbox Defense answers

### 1)
```scss
#water {
  grid-column-start: 3;
}
```

### 2)
```scss
#poison {
  grid-column-start: 5;
}
```

### 3)
```scss
#water {
  grid-column-end: 4;
}
```

### 4)
```scss
#water {
  grid-column-end: 2;
}
```

### 5)
```scss
#water {
  grid-column-end: -2;
}
```

### 6)
```scss
#poison {
  grid-column-start: -3;
}
```

### 7)
```scss
#water {
  grid-column-end: span 2;
}
```

### 8)
```scss
#water {
  grid-column-end: span 5;
}
```

### 9)
```scss
#water {
  grid-column-start: span 3;
}
```

### 10)
```scss
#water {
  grid-column: 4 / 6;
}
```

### 11)
```scss
#water {
  grid-column: 2 / 5;
}
```

### 12)
```scss
#water {
  grid-row-start: 3;
}
```

### 13)
```scss
#water {
  grid-row: 3 / 6;
}
```

### 14)
```scss
#poison {
  grid-row: 5 / 6;
  grid-column: 2 / 3;
}
```

### 15)
```scss
#water {
  grid-column: 2 / 6;
  grid-row: 1 / 6;
}
```

### 16)
```scss
#water {
  grid-area: 1 / 2 / 4 / 6;
}
```

### 17)
```scss
#water-2 {
  grid-area: 2 / 3 / 5 / 6;
}
```

### 18)
```scss
#poison {
  order: 5;
}
```

### 19)
```scss
.poison {
  order: -1;
}
```

### 20)
```scss
#garden {
  grid-template-columns: 50% 50%;
}
```

### 21)
```scss
#garden {
  grid-template-columns: repeat(8, 12.5%);
}
```

### 22)
```scss
#garden {
  grid-template-columns: 100px 3em 40%;
}
```

### 23)
```scss
#garden {
  grid-template-columns: 1fr 5fr;
}
```

### 24)
```scss
#garden {
  grid-template-columns: 50px 1fr 1fr 1fr 50px;
}
```

### 25)
```scss
#garden {
  grid-template-columns: 75px 3fr 2fr;
}
```

### 26)
```scss
#garden {
  grid-template-rows: 50px 0 0 0 1fr
}
```

### 27)
```scss
#garden {
  grid-template: 60% 40% / 200px;
}
```

### 28)
```scss
#garden {
  grid-template: 1fr 50px / 1fr 4fr;
}
```