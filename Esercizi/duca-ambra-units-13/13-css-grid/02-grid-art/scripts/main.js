/**
 * @file gallery.js
 * @author Ambra
 * @description This JavaScript file implements a dynamic image gallery with random grid placements and sizes
 * The gallery:
 * * - is created by dynamically generating HTML elements for each image in the array, setting their grid placement and size randomly
 * - allows for enlarging and shrinking images on click
 * - reshuffles the layout every n seconds
 * - stops reshuffling when hovering over an image and resumes reshuffling when the mouse is moved out
 * - has event listeners for enlarging/shrinking and stopping/resuming reshuffling
 */

// Array of image paths relative to root folder
const images = [
  './assets/blue-velvet-beksinski.jpg',
  './assets/crawler-beksinski.jpg',
  './assets/headless-horse-beksinski.jpg',
  './assets/horserider-beksinski.webp',
  './assets/knuckle-beksinski.jpg',
  './assets/wetnurse-beksinski.jpg',
  './assets/moon-tree-beksinski.jpg',
  './assets/monolith-face-beksinski.jpg',
  './assets/bat-beksinski.jpg',
  './assets/amalgam-run-beksinski.jpg',
];

const gallery = document.querySelector('.gallery');
let reshuffleInterval; // Initializing variable for interval clears

/**
 * Function to dynamically create the gallery with random grid placements and sizes
 * @returns {void}
 * - Clear the current images in the gallery
 * - Loop through each image in the array
 * - Create an image element for each image
 * - Set the source of the image element to the image path
 * - Randomly assign the image size within the grid by setting the grid column and row span
 * - Add a click event listener to the image element to enlarge/shrink the image
 * - Add mouseover and mouseout event listeners to the image element to stop/resume reshuffling
 * - Append the image element to the gallery
 */
function createRandomGallery() {
  gallery.innerHTML = ''; // Clear current images
  images.forEach((src) => {
      const img = document.createElement('img');
      img.src = src;

      // Randomly assign image size within grid
      const colSpan = Math.floor(Math.random() * 2) + 1; // Span 1-2 columns
      const rowSpan = Math.floor(Math.random() * 2) + 1; // Span 1-2 rows

      img.style.gridColumnEnd = `span ${colSpan}`;
      img.style.gridRowEnd = `span ${rowSpan}`;

      // Add click event to enlarge image
      img.addEventListener('click', () => enlargeImage(img));

      // Add mouseover event to stop reshuffling when hovering over an image
      img.addEventListener('mouseover', stopReshuffling);
      img.addEventListener('mouseout', resumeReshuffling);

      // Append image to the gallery
      gallery.appendChild(img);
  });
  console.log('Gallery created'); // debug
  console.log('Reshuffle interval:', reshuffleInterval); // debug
}

/**
 * Function to enlarge or shrink the image on click
 * @param {HTMLImageElement} image - The image element to enlarge or shrink
 * @returns {void}
 * - Toggle the 'enlarged' class on the clicked image
 * - Update the isEnlarged variable based on whether the 'enlarged' class is present on the image
 * - If the image is enlarged, clear the reshuffle interval to stop reshuffling and add the 'no-hover' class to
 *   all other images to disable the hover effect
 * - If the image is shrunk, start the reshuffle interval to resume reshuffling and remove the 'no-hover' class
 *   from all other images to enable the hover effect
 */
function enlargeImage(image) {
  const allImages = gallery.querySelectorAll('img');

  // Toggle the 'enlarged' class on the clicked image
  image.classList.toggle('enlarged');

  // Update the isEnlarged variable
  isEnlarged = image.classList.contains('enlarged');

  // Pause or resume reshuffling based on whether the image is enlarged
  if (isEnlarged) {
    clearInterval(reshuffleInterval); // Pause reshuffling while the image is enlarged
    allImages.forEach(img => {
      if (img !== image) img.classList.add('no-hover'); // Disable hover effect for other images
    });
    console.log('Reshuffling stopped'); // debug
  }
}

// Function to reshuffle gallery layout every n seconds
function startReshuffle() {
  reshuffleInterval = setInterval(createRandomGallery, 4000); // Change layout every 4 seconds
  console.log('Reshuffling interval started'); // debug
}

// Function to stop reshuffling
function stopReshuffling() {
  clearInterval(reshuffleInterval); // Stop reshuffling on mouse over
  console.log('Reshuffling stopped'); // debug
}

// Function to resume reshuffling
function resumeReshuffling() {
  startReshuffle(); // Resume reshuffling on mouse out
  console.log('Reshuffling started'); // debug
}

// Initialize the gallery and start reshuffling
createRandomGallery();
startReshuffle();