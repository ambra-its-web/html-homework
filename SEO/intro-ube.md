il nome dell'immagine in una figure deve essere uguale all'h2 se, esempio, sito beatles con membri in diverse section. il link alla pagina sarà nella figure stessa, questo si chiama **COLLEGAMENTO ORIZZONTALE**, fondamentale per SEO

---
## i json-ld (linked data)
il link ad essi va nell'head, così il browser prima legge questo invece dell'html e js

 del sito per la SEO è meglio farli separati per ambito, non tutto in uno. ne creo molteplici

sono utili perché permettono al browser di capire, ad esempio, cosa sia un "ricetta" nel tuo sito (e chi non usa una semantica perfetta nel codice) e sfruttarlo per l'indicizzazione

gli dici di ENFATIZZARE determinati elementi pure FUORI dal sito collegandoli ad oggetti con l'img di riferimento ed il link alla pagina

ad esempio del prof, i suoi oggetti nel json-ld

### "WebPageElement"
 per ogni macrocategoria del sito (quelle del menù a lato di [selmi](https://www.selmi-group.it/))
```json
"@type": "WebPageElement",
"name": "blablabla"
```
dico a google cosa sia quel che ho sul sito

utilissimo mettere le FAQ nel json

esempio: "qual'è 

esistono siti per fare il merge dei json, ma meglio separati lo stesso

### "WebSite"

le **keywords** ormai si usano solo per gli annunci. sono penalizzate come tentativo di forzatura ormai

### "BreadCrumb"
Importanti. Se sono lunghissime, qualcosa non va, max 3 passaggi (meglio immediato per l'utente)

### "LocalBusiness"
Utile per far capire se è vicino
Google genera una scheda di GoogleBusiness per ogni business, puoi richiederla per aggiungere informazioni. Le usa per dire quando è aperto, dove

Posso anche dargli info diverse, tipo un'immagine che non è quella del logo ma di un prodotto

Gli do anche le coordinate,
```json
"geo": {
    "@type": "GeoCoordinates"
}
```

### Cose che mi sono persaaa

### 

Nel JSON (INTERNO ALL'HTML) di una macrocategoria, posso mettere i json dei prodotti

essi sicuramente dovranno avere il segnalatore di poter aprire la loro pagina separata

Si possono mettere le review. Lui le ha messe finte nel [sito delle pompe sommerse di telid](https://www.telid.it/pompe-sommerse.html)

1. sito
2. pagina
3. azienda
4. resto (dettagli)

L'ordine mentale è conseguenza diretta di quello grafico

non banalizzare gli a capo, tab, spazi

questo per MANTENERE UN FORMAT. permette di trovare e sostituire su più file contemporaneamente senza farlo uno alla volta se uso gli stessi nomi

### Link canonici e alt
subito dopo i json-LD, avrò link di pagine uguali all'originale perché sono versioni di altre lingue

```html
</script>
<link rel="canonical" href="bla.it"> PRIMO

<link rel="alternate" hreflang="it" href="bla.it"> SECONDO
<link rel="alternate" hreflang="x-default" href="bla.it"> TERZO
```
si ripete ammerda, ma google vuole così

```html
<meta name="robots" content="index, follow">
```
index dice di indicizzare tutti i link presenti sulla pagina in base al tema attuale, follow diche che i link della pagina vanno seguiti

il `no/index` dice di non indicizzare gli altri link e concentrarsi su quello specifico del sito

utile se sono sulla pagina di uno specifico prodotto e voglio il focus su quello

general json/dl sul sito della [merkle](https://technicalseo.com/tools/schema-markup-generator/)

ricordati il [markup valildator per i json](https://technicalseo.com/tools/schema-markup-generator/)

```html
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="robe">
<meta name="twitter:site" content="pagina del sito">
<meta name="twitter:description" content="descrizione">
<meta name="twitter:image" content="link immagine social">
<meta name="twitter:image:alt" content="testo alt">
```
qui serve per dire (robe)

[google location changer](https://seranking.com/google-location-changer.html) per vedere dati ricerche da altri paesi senza vpn

sito per vedere [dati ancora più approfonditi sulle prestazioni della pagina](https://pagespeed.web.dev/)


---

sta facendo il sito di [refly](https://www.refly.org/)

ha visto quello di [montura](https://www.montura.com/it?srsltid=AfmBOopYeR1v1s5L5Fucrn2w59BuMwIpDCBGf4fiZ5oZHV_8M-to8Cf0)

aveva "montura store" come meta description. anzi, ne aveva due, e diverse! poi gli h2 contengono parole che NESSUNO in italia cercherebbe mai (esempio: "cycling")