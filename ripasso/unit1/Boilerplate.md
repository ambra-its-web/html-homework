### Attempt 1

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Attempt 1</title>
    <meta
      name="description"
      content="first attempt at typing out a boilerplate from memory"
    />
    <meta name="author" content="Ambra Duca" />
    <meta name="size" content="small" />
    <link rel="stylesheet" href="./styles/reset.css" />
    <link rel="stylesheet" href="./styles/style.css" />
  </head>
  <body>
    <h1>Attempt 1</h1>
    <!-- end of body -->
    <script src="./scripts/main.js"></script>
  </body>
</html>
```

Ho guardato troppe volte

### Attempt 2

<!doctype html>
<html lang="en">
	<head>
			<meta charset='utf-8'> X
			<title>Attempt 2</title>
			<meta name="description" content="attempt 2">			<meta name="author" content="Ambra Duca">
			
TOO MUCH GUIDANCE

### Attempt 5 (PERFECT no looksies)

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>attempt 5</title>
    <meta name="description" content="attempt 5" />
    <meta name="author" content="Ambra Duca" />
    <meta name="size" content="small" />
    <link rel="stylesheet" href="./styles/reset.css" />
    <link rel="stylesheet" href="./styles/style.css" />
  </head>
  <body>
    <h1>Attempt 5</h1>

    <!-- end of body -->
    <script src="./scripts/main.js"></script>
  </body>
</html>
```
